# Check if docker is running
if ! docker info >/dev/null 2>&1; then
    echo "Docker does not seem to be running, run it first and retry"
    exit 1
fi

ARQUIVO='./dados.txt'
count=1
#NOVALINE[0]=""
while read linha; do
    count=$[ $count + 1 ]
    NOVALINE$count="$linha"
done < "$ARQUIVO"