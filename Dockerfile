FROM alpine:3.14.1
RUN apk add --no-cache \
        python3 \
        py3-pip \
        curl \
    && pip3 install --upgrade pip \
    && pip3 install \
        awscli \
    && rm -rf /var/cache/apk/*
COPY . /app
WORKDIR /app
ENTRYPOINT [ "sh", "update-ip.sh" ] 