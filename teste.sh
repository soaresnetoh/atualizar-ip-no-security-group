#!/bin/bash

## VARIABLES
#MYIPV4=[ $1 = "meu_ip" ] && { curl -sX GET https://v4.ident.me; exit 0; } || { $1; exit 0; }  # Set my actual public IPv4
#MYIPV4=[ $1 -eq "meu_ip" ] && { echo "igual"; exit 0; }
if [[ $1 == "meu_ip" ]]
then 
   MYIPV4=$(curl -sX GET https://v4.ident.me)
else
   MYIPV4=$1
fi
echo $MYIPV4