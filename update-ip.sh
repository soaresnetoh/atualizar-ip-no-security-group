#!/bin/bash

## VARIABLES

if [[ $5 == "meu_ip" ]] # Set my actual public IPv4
then 
   MYIPV4=$(curl -sX GET https://v4.ident.me)
else
   MYIPV4=$5
fi

SGID=$1              # Set Security Group ID
INGPORT="$2"         # Set port to add on security group
REGION="$3"          # Set region to execute script
PROTOCOL=tcp         # Set protocol to use on rule
IPV4DESCRIPTION="$4" # Set description of IPv4
#MYIPV6            = $(curl -sX GET https://v6.ident.me)  # Set my actual public IPv6
# IPV6DESCRIPTION = "$5"                                 # Set description of IPv6


# Collect actual IPV4 based on description
IPV4_SGIP=$(aws ec2 describe-security-groups --group-id $SGID --output text --query \
          "SecurityGroups[].IpPermissions[?FromPort == \`$INGPORT\`].[IpRanges[?Description == '$IPV4DESCRIPTION'].[CidrIp]]" --output text --region $REGION)
echo $IPV4_SGIP >> IPV4_SGIP.txt 

# Collect actual IPV6 based on description
# IPV6_SGIP=$(aws ec2 describe-security-groups --group-id $SGID --output text --query "SecurityGroups[].IpPermissions[?FromPort == \`$INGPORT\`].[Ipv6Ranges[?Description == '$IPV6DESCRIPTION'].[CidrIpv6]]" --output text --region $REGION)
# echo $IPV6_SGIP
## SCRIPT
if [ "$MYIPV4/32" != "$IPV4_SGIP" ]; then

    # Remove old IPv4 ingress rule only if already exist one.
    if [ "$IPV4_SGIP" != "" ]; then 
        aws ec2 revoke-security-group-ingress --group-id $SGID --protocol $PROTOCOL --port $INGPORT --cidr $IPV4_SGIP --region $REGION
    fi
 
    # Add new IPv4 rule
    aws ec2 authorize-security-group-ingress \
        --group-id $SGID \
        --ip-permissions IpProtocol=$PROTOCOL,FromPort=$INGPORT,ToPort=$INGPORT,IpRanges="[{CidrIp=$MYIPV4/32,Description='$IPV4DESCRIPTION'}]" \
        --region $REGION
    if [ "$?" -eq 0 ]; then
        echo " » Endereço IPV4 atualizado com sucesso no security group."
    else
        echo " » Ocorreu algum problema ao atualizar o seu IPv4 no destino" #, tentaremos atualizar o seu IPV6 porém entre em contate com o administrador para verificar este problema."
    fi
else
    echo " » O seu IPv4 atual e o presente no security group são os mesmos portanto não será realizado nenhuma atualização."
fi

# if [ "$MYIPV6/128" != "$IPV6_SGIP" ]; then

#     # Remove old IPv6 ingress rule only if already exist one.
#     if [ "$IPV6_SGIP" != "" ]; then
#         aws ec2 revoke-security-group-ingress --group-id $SGID --ip-permissions "[{\"IpProtocol\": \"$PROTOCOL\", \"FromPort\": $INGPORT, \"ToPort\": $INGPORT, \"Ipv6Ranges\": [{\"CidrIpv6\": \"$IPV6_SGIP\"}]}]" --region $REGION
#     fi

#     aws ec2 authorize-security-group-ingress \
#         --group-id $SGID \
#         --ip-permissions IpProtocol=$PROTOCOL,FromPort=$INGPORT,ToPort=$INGPORT,Ipv6Ranges="[{CidrIpv6=$MYIPV6/128,Description='$IPV6DESCRIPTION'}]"
#     if [ "$?" -eq 0 ]; then
#         echo " » Endereço IPV6 atualizado com sucesso no security group."
#     else
#         echo " » Ocorreu algum problema ao atualizar o seu IPv6 no destino, contate o administrador para verificar o ocorrido."
#     fi
# else
#     echo " » O seu IPv6 atual e o presente no security group são os mesmos portanto não será realizado nenhuma atualização."
# fi